#include "main.h"

void utils_beep(int freq, int duration)
{
    Beep(freq, duration);
}

void utils_sleep(int ms)
{
    Sleep(ms);
}
