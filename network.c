#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static WSADATA wsa_data;

void network_init(void)
{
    if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
    {
        console_printf("error: failed to initialize sockets\n");
        exit(EXIT_FAILURE);
    }
}

void network_finalize(void)
{
    WSACleanup();
}

bool network_connect(const char *host, uint16_t port, int timeout)
{
    struct addrinfo hints;
    struct addrinfo *result = NULL;
    SOCKET sock = INVALID_SOCKET;
    int status = 0;
    bool return_value = true;
    char port_str[6] = {0};
    fd_set write_set, except_set;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    snprintf(port_str, 6, "%u", port);
    if ((getaddrinfo(host, port_str, &hints, &result)) != 0)
        return false;

    if ((sock = socket(result->ai_family, result->ai_socktype, result->ai_protocol)) == INVALID_SOCKET)
        goto fail;

    struct timeval to;
    struct timeval *to_ptr;
    if (timeout == 0)
    {
        to.tv_sec = 0;
        to.tv_usec = 0;
        to_ptr = &to;
    }
    else if (timeout < 0)
    {
        // infinite
        to_ptr = NULL;
    }
    else
    {
        to.tv_sec = timeout / 1000;
        to.tv_usec = (timeout % 1000) * 1000;
        to_ptr = &to;
    }

    u_long non_blocking = 1;
    if (ioctlsocket(sock, FIONBIO, &non_blocking) == SOCKET_ERROR)
        goto fail;

    if (connect(sock, result->ai_addr, result->ai_addrlen) == SOCKET_ERROR)
    {
        if (WSAGetLastError() != WSAEWOULDBLOCK)
            goto fail;

        FD_ZERO(&write_set);
        FD_ZERO(&except_set);

        FD_SET(sock, &write_set);
        FD_SET(sock, &except_set);

        if (select(0, NULL, &write_set, &except_set, to_ptr) <= 0)
            goto fail;

        if (FD_ISSET(sock, &except_set))
            goto fail;
    }

    freeaddrinfo(result);
    closesocket(sock);

finish:
    return return_value;

fail:
    if (result != NULL)
        freeaddrinfo(result);
    if (sock != INVALID_SOCKET)
        closesocket(sock);
    return_value = false;
    goto finish;
}
