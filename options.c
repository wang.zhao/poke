#include "main.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_TIMEOUT 300000
#define DEFAULT_HOST "google.com"
#define DEFAULT_PORT 80
#define DEFAULT_COUNT 0
#define DEFAULT_TIMEOUT 3000
#define DEFAULT_DELAY 1000
#define DEFAULT_BEEP (BEEP_MODE_NONE)

static void do_help(void)
{
    printf(
        "Usage: poke [options] [[host] [port]]\n"
        "\n"
        "Tests TCP connection to host and port.\n"
        "\n"
        "Options:\n"
        "  /?          Show this message and exit\n"
        "                Aliases: /help -h --help\n"
        "  /v          Show version information\n"
        "                Aliases: /ver /version -v --version\n"
        "  /h HOST     Hostname or IP address to connect to\n"
        "                Default: %s\n"
        "                Aliases: /host -H --host\n"
        "  /p PORT     Port number number to connect to\n"
        "                Min: 1, Max: 65535, Default: %d\n"
        "                Aliases: /port -p --port\n"
        "  /c COUNT    Number of tests to run\n"
        "                Default: %d, Infinite: 0\n"
        "                Aliases: /count -c --count\n"
        "  /t TIMEOUT  Connection timeout in milliseconds\n"
        "                Default: %d, Infinite: 0\n"
        "                Aliases: /to /timeout -t --timeout\n"
        "  /d DELAY    Delay between tests in milliseconds\n"
        "                Default: %d\n"
        "                Aliases: /delay -d --delay\n"
        "  /b          Make beep noise on success or failure\n"
        "                Aliases: /beep -b --beep\n"
        "\n"
        "Examples:\n"
        "  Check for internet connection:\n"
        "    > poke\n"
        "  Check if Wikipedia website is online:\n"
        "    > poke wikipedia.org\n"
        "  Check for internet DNS connection:\n"
        "    > poke 8.8.8.8 53\n"
        "  Check local IP address on port 123:\n"
        "    > poke 192.168.0.42 123\n"
        "\n"
        "Common Ports:\n"
        "  FTP              : 21\n"
        "  SSH              : 22\n"
        "  SMTP             : 25\n"
        "  DNS              : 53\n"
        "  HTTP             : 80\n"
        "  POP3             : 110\n"
        "  HTTP/SSL/TLS     : 443\n"
        "  RTSP             : 554\n"
        "  IMAP             : 993\n"
        "\n"
        "Written by Matthew Brush <m@cdbz.ca>",
        DEFAULT_HOST, DEFAULT_PORT, DEFAULT_COUNT, DEFAULT_TIMEOUT, DEFAULT_DELAY);
#ifndef _WIN32
    fputc('\n', stdout);
#endif
    exit(EXIT_SUCCESS);
}

static void do_version(void)
{
    printf("poke v0.1\n");
    exit(EXIT_SUCCESS);
}

static int parse_int(const char *str, int min_val, int max_val, bool *ok)
{
    char *end = NULL;
    errno = 0;
    long val = strtol(str, &end, 10);
    if (errno != 0 || *end != '\0')
    {
        *ok = false;
        return -1;
    }
    else if (val < min_val || val > max_val)
    {
        *ok = false;
        return -2;
    }
    else
    {
        *ok = true;
        return val;
    }
}

static void debug_options(const struct options *opts)
{
#if 0
    printf("Options:\n"
           "  host    : %s\n"
           "  port    : %d\n"
           "  count   : %d\n"
           "  timeout : %d\n"
           "  delay   : %d\n"
           "  beep    : %d\n",
           opts->host, opts->port, opts->count, opts->timeout, opts->delay, opts->beep);
#else
    (void)opts;
#endif
}

static inline bool str_equal(const char *s1, const char *s2)
{
    return (strcmp(s1, s2) == 0);
}

static int str_case_cmp(const char *s1, const char *s2)
{
    const unsigned char *p1 = (const unsigned char *)s1;
    const unsigned char *p2 = (const unsigned char *)s2;
    int result;
    if (p1 == p2)
        return 0;
    while ((result = tolower(*p1) - tolower(*p2++)) == 0)
        if (*p1++ == '\0')
            break;
    return result;
}

static bool str_case_equal(const char *s1, const char *s2)
{
    return (str_case_cmp(s1, s2) == 0);
}

void options_parse(struct options *opts, int argc, char **argv)
{
    bool have_host = false;
    bool have_port = false;
    bool have_test = false;
    bool have_count = false;
    bool have_timeout = false;
    bool have_delay = false;
    bool have_beep = false;

    memset(opts, 0, sizeof(struct options));

    // TODO: make these user-specified later
    opts->test = TEST_TCP_CONNECT;
    have_test = true;

    // parse the arguments
    int i;
    for (i = 1; i < argc; i++)
    {
        const char *arg = argv[i];

        if (str_case_equal(arg, "/?") || str_case_equal(arg, "/help") || str_equal(arg, "-h") || str_equal(arg, "--help"))
        {
            do_help();
            return;
        }
        else if (str_case_equal(arg, "/v") || str_case_equal(arg, "/ver") || str_case_equal(arg, "/version") || str_equal(arg, "-v") || str_equal(arg, "--version"))
        {
            do_version();
            return;
        }
        else if (str_case_equal(arg, "/h") || str_case_equal(arg, "/host") || str_equal(arg, "-H") || str_equal(arg, "--host"))
        {
            if (have_host)
            {
                fprintf(stderr, "error: host already specified\n");
                exit(EXIT_FAILURE);
            }
            else if ((i + 1) >= argc)
            {
                fprintf(stderr, "error: missing host argument\n");
                exit(EXIT_FAILURE);
            }
            else
            {
                opts->host = strdup(argv[i + 1]);
                have_host = true;
                i++;
            }
        }
        else if (str_case_equal(arg, "/p") || str_case_equal(arg, "/port") || str_equal(arg, "-p") || str_equal(arg, "--port"))
        {
            if (have_port)
            {
                fprintf(stderr, "error: port already specified\n");
                exit(EXIT_FAILURE);
            }
            else if ((i + 1) >= argc)
            {
                fprintf(stderr, "error: missing port argument\n");
                exit(EXIT_FAILURE);
            }
            else
            {
                bool ok = false;
                int val = parse_int(argv[i + 1], 1, 65535, &ok);
                if (!ok)
                {
                    if (val == -2)
                        fprintf(stderr, "error: port value '%s' is out of range (1-65535)\n", argv[i + 1]);
                    else
                        fprintf(stderr, "error: invalid port value '%s'\n", argv[i + 1]);
                    exit(EXIT_FAILURE);
                }
                else
                {
                    opts->port = val;
                    have_port = true;
                    i++;
                }
            }
        }
        else if (str_case_equal(arg, "/b") || str_case_equal(arg, "/beep") || str_equal(arg, "-b") || str_equal(arg, "--beep"))
        {
            if (have_beep)
            {
                fprintf(stderr, "error: beep is already specified\n");
                exit(EXIT_FAILURE);
            }
            else
            {
                opts->beep = BEEP_MODE_ALL;
                have_beep = true;
            }
        }
        else if (str_case_equal(arg, "/c") || str_case_equal(arg, "/count") || str_equal(arg, "-c") || str_equal(arg, "--count"))
        {
            if (have_count)
            {
                fprintf(stderr, "error: count is already specified\n");
                exit(EXIT_FAILURE);
            }
            else if ((i + 1) >= argc)
            {
                fprintf(stderr, "error: missing count argument\n");
                exit(EXIT_FAILURE);
            }
            else
            {
                bool ok = false;
                int val = parse_int(argv[i + 1], 0, INT_MAX, &ok);
                if (!ok)
                {
                    if (val == -2)
                        fprintf(stderr, "error: count value '%s' is out of range (0-%d)\n", argv[i + 1], INT_MAX);
                    else
                        fprintf(stderr, "error: invalid count value '%s'\n", argv[i + 1]);
                    exit(EXIT_FAILURE);
                }
                else
                {
                    opts->count = val;
                    have_count = true;
                    i++;
                }
            }
        }
        else if (str_case_equal(arg, "/t") || str_case_equal(arg, "/to") || str_case_equal(arg, "/timeout") || str_equal(arg, "-t") || str_equal(arg, "--timeout"))
        {
            if (have_timeout)
            {
                fprintf(stderr, "error: timeout is already specified\n");
                exit(EXIT_FAILURE);
            }
            else if ((i + 1) >= argc)
            {
                fprintf(stderr, "error: missing timeout argument\n");
                exit(EXIT_FAILURE);
            }
            else
            {
                bool ok = false;
                int val = parse_int(argv[i + 1], 1, MAX_TIMEOUT, &ok);
                if (!ok)
                {
                    if (val == -2)
                        fprintf(stderr, "error: timeout value '%s' is out of range (1-%d)\n", argv[i + 1], MAX_TIMEOUT);
                    else
                        fprintf(stderr, "error: invalid timeout value '%s'\n", argv[i + 1]);
                    exit(EXIT_FAILURE);
                }
                else
                {
                    opts->timeout = val;
                    have_timeout = true;
                    i++;
                }
            }
        }
        else if (str_case_equal(arg, "/d") || str_case_equal(arg, "/delay") || str_equal(arg, "-d") || str_equal(arg, "--delay"))
        {
            if (have_delay)
            {
                fprintf(stderr, "error: delay is already specified\n");
                exit(EXIT_FAILURE);
            }
            else if ((i + 1) >= argc)
            {
                fprintf(stderr, "error: missing delay argument\n");
                exit(EXIT_FAILURE);
            }
            else
            {
                bool ok = false;
                int val = parse_int(argv[i + 1], 0, INT_MAX, &ok);
                if (!ok)
                {
                    if (val == -2)
                        fprintf(stderr, "error: delay value '%s' is out of range (0-%d)\n", argv[i + 1], INT_MAX);
                    else
                        fprintf(stderr, "error: invalid delay value '%s'\n", argv[i + 1]);
                    exit(EXIT_FAILURE);
                }
                else
                {
                    opts->delay = val;
                    have_delay = true;
                    i++;
                }
            }
        }
        else
        {
            // no more options
            break;
        }
    }

    // TODO: parse trailing host/port
    if (i < argc)
    {
        // only host or port left (not both)
        if (((i + 1) == argc) && (!have_host || !have_port))
        {
            // port only
            if (have_host)
            {
                bool ok = false;
                int val = parse_int(argv[i], 1, 65535, &ok);
                if (!ok)
                {
                    if (val == -2)
                        fprintf(stderr, "error: port value '%s' is out of range (1-65535)\n", argv[i]);
                    else
                        fprintf(stderr, "error: invalid port value '%s'\n", argv[i]);
                    exit(EXIT_FAILURE);
                }
                else
                {
                    opts->port = val;
                    have_port = true;
                }
            }
            // host only, or port only
            else
            {
                bool full_numeric = true;
                for (const char *p = argv[i]; *p; p++)
                {
                    if (!isdigit(*p))
                    {
                        full_numeric = false;
                        break;
                    }
                }
                if (full_numeric)
                {
                    bool ok = false;
                    int val = parse_int(argv[i], 1, 65535, &ok);
                    if (!ok)
                    {
                        if (val == -2)
                            fprintf(stderr, "error: port value '%s' is out of range (1-65535)\n", argv[i]);
                        else
                            fprintf(stderr, "error: invalid port value '%s'\n", argv[i]);
                        exit(EXIT_FAILURE);
                    }
                    else
                    {
                        opts->port = val;
                        have_port = true;
                    }
                }
                else
                {
                    opts->host = strdup(argv[i]);
                    have_host = true;
                }
            }
        }
        else if (((i + 2) == argc) && !have_host && !have_port)
        {
            // host and port
            opts->host = strdup(argv[i]);
            have_host = true;

            bool ok = false;
            int val = parse_int(argv[i + 1], 1, 65535, &ok);
            if (!ok)
            {
                if (val == -2)
                    fprintf(stderr, "error: port value '%s' is out of range (1-65535)\n", argv[i + 1]);
                else
                    fprintf(stderr, "error: invalid port value '%s'\n", argv[i + 1]);
                exit(EXIT_FAILURE);
            }
            else
            {
                opts->port = val;
                have_port = true;
            }
        }
        else
        {
            fprintf(stderr, "error: extra unknown arguments found\n");
            exit(EXIT_FAILURE);
        }
    }

    if (!have_host && !have_port)
    {
        opts->host = strdup(DEFAULT_HOST);
        opts->port = DEFAULT_PORT;
    }
    else if (have_host && !have_port)
    {
        opts->port = DEFAULT_PORT;
    }
    else if (!have_host && have_port)
    {
        opts->host = strdup(DEFAULT_HOST);
    }

    if (!have_test)
        opts->test = TEST_TCP_CONNECT;

    if (!have_count)
        opts->count = DEFAULT_COUNT;

    if (!have_timeout)
        opts->timeout = DEFAULT_TIMEOUT;
    if (opts->timeout == 0)
        opts->timeout = -1;

    if (!have_delay)
        opts->delay = DEFAULT_DELAY;

    if (!have_beep)
        opts->beep = DEFAULT_BEEP;

    debug_options(opts);
}
