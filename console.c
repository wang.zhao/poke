#include "main.h"

#include <stdlib.h>
#include <stdio.h>

static HANDLE g_hConsole;
static CONSOLE_SCREEN_BUFFER_INFO g_defaultConsoleAttributes;

void console_init(void)
{
    g_hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    GetConsoleScreenBufferInfo(g_hConsole, &g_defaultConsoleAttributes);

    atexit(console_reset);
}

void console_finalize(void)
{
}

void console_reset(void)
{
    SetConsoleTextAttribute(g_hConsole, g_defaultConsoleAttributes.wAttributes);
}

void console_set_color(int color)
{
    SetConsoleTextAttribute(g_hConsole, FOREGROUND_GREEN);
}

void console_printf(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stdout, fmt, ap);
    va_end(ap);
}
